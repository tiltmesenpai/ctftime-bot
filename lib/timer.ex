defmodule Timer.Supervisor do
  use Supervisor

  def start_link do
    Supervisor.start_link(__MODULE__, :ok)
  end

  def init(:ok) do
    children = [
      worker(Timer.GenServer, [Timer.GenServer])
    ]
    supervise(children, strategy: :one_for_one)
  end
end

defmodule Timer.GenServer do
  use GenServer

  def start_link(name) do
    GenServer.start_link(__MODULE__, [], name: name)
  end

  def every(link, fun, interval, name) do
    GenServer.cast(link, {:every, fun, interval, name})
  end

  def stop(link, name) do
    GenServer.cast(link, {:stop, name})
  end

  def handle_cast({:every, fun, interval, name}, list) do
    Process.send(self(), {:tick, fun, interval, name}, [])
    {:noreply, [name | list]}
  end

  def handle_info({:tick, fun, interval, name}, list) do
    if Enum.member?(list, name) do
      Process.send_after(self(), {:tick, fun, interval, name}, interval)
      fun.()
      {:noreply, list}
    else
      {:noreply, list}
    end
  end
end
