defmodule CtfTime do
  use Alchemy.Events
  alias Alchemy.Client

  Events.on_ready(:start_loop)
  def start_loop(_id, _shards) do
    interval = Application.get_env(:ctf_time, :interval)
    IO.puts "Starting sync loop"
    Timer.GenServer.every(Timer.GenServer, &sync_events/0, interval, :ctf_time)
  end

  def sync_events do
    IO.puts "Syncing events"
    chan  = Application.get_env(:ctf_time, :channel)
    limit = Application.get_env(:ctf_time, :limit)
    {:ok, msgs}  = Client.get_messages(chan, limit: limit)
    diffs = msgs
            |> Enum.sort(fn(a, b) ->
              {:ok, a_time, _t} = a.timestamp |> DateTime.from_iso8601
              {:ok, b_time, _t} = b.timestamp |> DateTime.from_iso8601
              DateTime.compare(a_time, b_time) != :gt
              end)
            |> Diff.diff(get_events(), &eq_event/2)
            |> Stream.filter(&(&1 != :nop))
            |> Stream.each(fn(change) ->
              case change do
                {:edit, from, to} ->
                  IO.puts("Editing message")
                  {:ok, _msg} = Client.edit_embed(from, make_embed(to))
                {:delete, msg} ->
                  IO.puts "Deleting message"
                  {:ok, nil} = Client.delete_message(msg)
                {:insert, event} ->
                  IO.puts "Sending message"
                  {:ok, _msg} = Client.send_message(chan, "", embed: make_embed(event))
              end
            end)
            |> Enum.count
    IO.puts "Done. #{diffs} modifications made."
  end

  def get_events do
    limit    = Application.get_env(:ctf_time, :limit)
    lookback = Application.get_env(:ctf_time, :lookback)
    window   = Application.get_env(:ctf_time, :window)
    now      = DateTime.utc_now |> DateTime.to_unix
    IO.puts "Pulling from: /events/?limit=#{limit}&start=#{now - lookback}&finish=#{round(now + window)}"
    CtfTimeApi.get!("/events/?limit=#{limit}&start=#{now - lookback}&finish=#{round(now + window)}").body
    |> Enum.sort(&(DateTime.compare(&1.start, &2.start) != :gt))
  end

  def make_embed(event) do
    {_time, color} = List.keyfind(colors, event_current(event), 0) 
    alias Alchemy.Embed
    %Embed{}
    |> Embed.title(event[:title])
    |> Embed.url(event[:url])
    |> Embed.description(event[:description])
    |> Embed.thumbnail(event[:logo])
    |> Embed.field("Format", event[:format], inline: true)
    |> Embed.field("Duration", "#{event[:duration][:days]} days, #{event[:duration][:hours]} hours", inline: true)
    |> Embed.footer(text: "ctf_id: " <> inspect(event[:ctf_id]))
    |> Embed.timestamp(event[:start])
    |> Embed.color(color)
  end

  def event_current(event) do
    now = DateTime.utc_now()
    if DateTime.compare(now, event.start) == :gt do
      if DateTime.compare(now, event.finish) == :gt do
        :past
      else
        :present
      end
    else
      :future
    end
  end

  def eq_event(msg, event) do
    case msg.embeds do
      [embed | _embeds] -> 
        {_time, color} = List.keyfind(colors, event_current(event), 0)
        embed.footer.text == ("ctf_id: " <> inspect(event[:ctf_id])) and embed.color == color
      []                -> false
    end
  end

  def colors do
    [{:future, 0x00AAB5}, {:present, 0x25DEA1}, {:past, 0xDE2663}]
  end
end

defmodule CtfTimeApi do
  use HTTPoison.Base

  def process_url(url) do
    "https://ctftime.org/api/v1" <> url
  end

  def process_response_body(body) do
    body 
    |> Poison.decode!(keys: :atoms)
    |> Enum.map(fn(event) ->
      {:ok, start,  _t}  = event[:start]  |> DateTime.from_iso8601
      {:ok, finish, _t}    = event[:finish] |> DateTime.from_iso8601
      %{event | start: start, finish: finish}
    end)
  end

  def active?(event) do
    now = DateTime.utc_now()
    DateTime.compare(now, event[:start]) == :gt and DateTime.compare(now, event[:finish]) == :lt
  end
end

defmodule Bot do

  use Application
  alias Alchemy.Client

  def start(_type, _args) do
    token = System.get_env("DISCORD_TOKEN")
    run = Client.start(token)
    Timer.Supervisor.start_link
    use CtfTime
    run
  end
end
