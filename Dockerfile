FROM elixir:slim
ADD _build/prod/rel/ctf_time/ /ctf_time
ENTRYPOINT ["/ctf_time/bin/ctf_time", "foreground"]
